---
title: "Exploring KEGG pathway results"
author: "Ann Loraine"
date: "June 1, 2016"
output: html_document
---

# Introduction

Rob Reid mapped blueberry genes assembled from RNA-Seq reads onto KEGG enzyme identifiers. He created a large file mapping expression values (FPKM) by sample onto these KEGG enzyme identifiers. 

This Markdown reads the file and performs simple exploratory data analysis.

# Methods and Analysis

Read the file:

```{r}
fname='../ExternalDataSets/MASTERTABLE-kaas-fpkm.txt.gz'
d=read.delim(fname,header=T,sep='\t',as.is=T)
```

The data set has `r nrow(d)` rows of `r ncol(d)` columns.

The first column (labeled `r names(d)[1]`) is the KEGG enzyme identifier. 


------------

Rename it for easier typing:

```{r}
names(d)[1]='enz'
```

There are `r length(unique(d$enz))` KEGG enzyme identifiers.

Are there missing values?

```{r}
v=apply(as.matrix(d[,2:ncol(d)]),2,function(x){any(is.na(x))})
```

There are missing values in: `r sum(v)` columns.

-------------


Make the KEGG id the row name, and remove that column.
```{r}
row.names(d) = d[,1]
d=d[2:ncol(d)]
```

### NA values
Do any rows have NA values?
```{r}
v=which(apply(d, 1, function(x){any(is.na(x))}))
d[v,]
```
I'm not sure how this row got in. In the text file, it looks like tab-delimited white space.  Remove this row.
```{r}
d=d[ setdiff(1:nrow(d),v) , ]
```

The data set has `r nrow(d)` rows of `r ncol(d)` samples.

### All zero counts
Do any rows have all-0 counts?
```{r}
dsum=rowSums(d)
w=which(dsum==0)
```
There are `r length(w)` rows with 0 counts in all samples.

I thought KEGG.ID's only got onto this table by being associated with a transcript generated from this data set... so if none of the samples had suppport for that transcript I don't see how we had the transcript sequence to get the KEGG.ID in the first place.







