#!/usr/bin/env python

"""Read and re-format the August 2015 genome assembly."""

ex=\
"""
Read and reformat Aug 2015 assembly sequence.

"""
import sys,argparse,fileinput,gzip,Bio

def main(fasta_file=None,min_size=0):
    d = {}
    wrote=0
    handle=getHandle(fasta_file=fasta_file)
    from Bio import SeqIO
    for record in SeqIO.parse(handle, "fasta"):
        new_name=getNewName(record)        
        if d.has_key(new_name):
            raise Exception("Two sequences with the same id %s"%record.id)
            # get the id from the header
            # convert to more reasonable format
            # write the record to stdout
            # find out the size
            # SeqIO.write(record, sys.stdout, "fasta")
        record.id=new_name
        d[new_name]=len(record)
        if d[new_name]>min_size:
            SeqIO.write(record,sys.stdout,"fasta")
            wrote=wrote+1
    sys.stderr.write("Wrote: %i records\n"%wrote)
    sys.stderr.write("Read: %i records\n"%len(d))
    handle.close()

def getNewName(record):
    # scaffold23|size778774
    return record.id.split('|')[0]

def getHandle(fasta_file=None):
    from Bio import SeqIO
    if fasta_file.endswith('.gz'):
        handle=gzip.GzipFile(fasta_file)
    else:
        handle = open(fasta_file, "rU")
    return handle

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument('-f','--fasta_file',metavar='FASTA_FILE',
                        help='Genome sequence file [required]')
    parser.add_argument('-m','--min_size',metavar='MININUM_SIZE',type=int,
                        default=0,
                        help='Minimum size for a contig [default is 0]')
    args = parser.parse_args()
    fasta_file=args.fasta_file
    min_size=args.min_size
    if fasta_file:
        main(fasta_file=fasta_file,min_size=min_size)
    else:
        parser.print_help()
