#!/bin/sh
# run in this directory

# assembly from Rob Reid
F='../../ExternalDataSets/blueberry-take6-scaffolds.fna.gz'
S='reformatAug2015Assembly.py'
M=2000
G='V_corymbosum_Aug_2015'
R='../results'
$S -m $M -f $F > $G.fa
faToTwoBit $G.fa $G.2bit
# to get read of description written by SeqIO from BioPython
twoBitToFa $G.2bit $G.fa
gzip -f $G.fa
twoBitInfo $G.2bit $G.txt
mv $G.fa.gz $R/.
mv $G.2bit $R/.
mv $G.txt $R/.
