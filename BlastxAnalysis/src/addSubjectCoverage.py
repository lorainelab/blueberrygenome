#!/usr/bin/env python

ex=\
"""
To assess whether a hit to a database entry is real, we need to know how much
of the matched (subject) protein was covered by the HSPs comprising the hit.
However, often times the HSPs overlap along the subject sequence, making it
difficult to compute percent coverage for a given subject. This script reads
blastx output from the big_blastx.py pipeline and calculates percent subject
coverage for each query and subject pair, adding the percent coverage as a final
field to every HSP.

Assumptions:
  field numbers.

"""

import sys,argparse,fileinput


# fields
# 0 database 
# 1 q.id
# 2 q.length
# 3 q.frame
# 4 q.start
# 5 q.end
# 6 s.length
# 7 s.start
# 8 s.end
# 9 identity
# 10 evalue
# 11 bit.score
# 12 s.acc.ver
# 13 s.title
qidF=1
sidF=12
iF=9
sstartF=7
sendF=8
titleF=13
slenF=6


def main(files=None):
    d = {}
    sep='\t'
    onfirst=True
    for line in fileinput.input(files):
        if onfirst:
            sys.stdout.write(line.rstrip()+'\ts.cov\n')
            onfirst=False
            continue
        toks=line.rstrip().split('\t')
        qid=toks[qidF]
        sid=toks[sidF]
        title=toks[titleF]
        if sid=='NA': # if query had no hit
            toks.append('NA')
            newline=sep.join(toks)+'\n'
            sys.stdout.write(line)
            continue
        else: # store the hit details
            if not d.has_key(qid):
                d[qid]={sid:[toks]}
            elif not d[qid].has_key(sid):
                d[qid][sid]=[toks]
            else:
                d[qid][sid].append(toks)
    for qid in d.keys():
        for sid in d[qid].keys():
            hsps=d[qid][sid]
            title=hsps[0][titleF]
            slen=int(hsps[0][slenF])
            s=[0]*slen # make empty array of size slen
            for hsp in hsps:
                stitle=hsp[titleF]
                if not title==stitle:
                    raise ValueError("Two different subjects per query %s"%qid)
                start=int(hsp[sstartF])-1
                end=int(hsp[sendF])
                for i in range(start,end):
                    s[i]=1
            scov=sum(s)/float(slen)*100
            for hsp in hsps:
                line='\t'.join(hsp)+'\t'+str(scov)+'\n'
                sys.stdout.write(line)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument('files', metavar='FILE', nargs='*',
                        help='BLASTX output file, tab-delimited')
    args = parser.parse_args()
    main(files=args.files)
