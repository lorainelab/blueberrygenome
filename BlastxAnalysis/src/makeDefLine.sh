#!/bin/bash

gunzip -c test_big_blastx_results.tsv.gz | ../src/addSubjectCoverage.py | ../src/addSummarizedIdentity.py | ../makeDefLine.py
