#!/bin/sh

# Get BlastTable.txt.gz - output from blastx search of nr, from Vikas
IN=BlastTable.txt.gz

# Fix header
gunzip -c $IN | parseBlastTable.py | gzip -c > BlastTable.tsv.gz 
