#!/usr/bin/env python

ex=\
"""
Write out a mapping of transcript to gene.
Reads from stdin or a file.
Writes to stdout.

example)
  gunzip -c V_corymbosum_scaffold_May_2013.bed.gz | makeTranscriptToGene.py | gzip -c > tx2gene.tsv.gz

By the way, a simple shell script using cut can do this.
"""

import fileinput,sys,optparse

def main(args=None):
    d = {}
    sys.stdout.write('transcript\tgene\n')
    for line in fileinput.input(args):
        toks = line.rstrip().split('\t')
        tx_id=toks[3]
        gene_id=toks[12]
        sys.stdout.write('%s\t%s\n'%(tx_id,gene_id))

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)
