#!/usr/bin/env python

ex=\
"""
Read blast results.
Fix header so that output can be easily analyzed in R.
Writes to stdout. Can read from stdin.

example)

  gunzip -c BlastTable.txt.gz | parseBlastTable.py | gzip -c > BlastTable.tsv.gz
"""

import fileinput,sys,optparse

def main(args=None):
    for line in fileinput.input(args):
        toks=line.rstrip().split('\t')
        # for some reason there are leading and trailing spaces in the header fields -- ?????
        toks=map(lambda x:x.strip(),toks)
        if fileinput.lineno()==1:
            if not toks[0]=='query':
                raise ValueError("Wrong input:%s."%toks[0])
            heads=['qid','sid','identity',toks[3],toks[4],toks[5],'qstart','qend','sstart',
                   'send','evalue','score','descr','species']
            sys.stdout.write('\t'.join(heads)+'\n')
        else:
            sys.stdout.write('\t'.join(toks)+'\n')

if __name__ == '__main__':
    usage='%prog [file ...]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)
