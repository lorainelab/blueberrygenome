#!/usr/bin/env python

ex=\
"""
Add gene id, parsed from the transcript id.
Reads from stdin or a file.
Writes to stdout.

example)

  gunzip -c BlastTableFixed.txt.gz | addGeneId.py -b V_corymbosum_scaffold_May_2013.bed.gz > BlastTableWithGeneId.tsv
"""

import fileinput,sys,optparse,gzip

def main(field=None,args=None,bed_file=None):
    d = {}
    if bed_file.endswith('.gz'):
        fh = gzip.GzipFile(bed_file)
    else:
        fh = open(bed_file)
    while 1:
        line = fh.readline()
        if not line:
            fh.close()
            break
        toks=line.split()
        tx_id=toks[3]
        gene_id=toks[12]
        d[tx_id]=gene_id
    for line in fileinput.input(args):
        toks = line.rstrip().split('\t')
        if fileinput.lineno()==1:
            toks.append('gene')
            sys.stdout.write('\t'.join(toks)+'\n')
        else:
            tx_id=toks[field]
            gene_id=d[tx_id]
            toks.append(gene_id)
            sys.stdout.write('\t'.join(toks)+'\n')

# this was helpful
# http://stackoverflow.com/questions/9756396/remove-parsed-options-and-their-values-from-sys-argv

if __name__ == '__main__':
    usage='%prog -b file [ -f FIELD ] [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    parser.add_option('-f','--field',help='Field with transcript id [default is 1]',
                      default=1,type="int",dest='field')
    parser.add_option('-b','--bed_file',help='Name of bed14 file with gene id in the 13th field [required]',
                      default=None,type="string",dest="bed_file")
    (options,args)=parser.parse_args()
    field=options.field-1 # convert from one-based
    main(field=field,bed_file=options.bed_file,args=args)
