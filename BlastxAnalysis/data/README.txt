20130630_final_cdna.refined.fa.gz:
        
        cDNA sequences for blueberry gene models

BlastTable.tsv.gz: 

         blastx results (from Vikas Gupta), blastx against protein nr
         database, done around July 2013. Formatting problems fixed w/
         ../src/parseBlastTable.py
 
big_blastx_results.tsv.gz

        contains output from searching blueberry genes against
        plant RefSeq databases using blastx; RefSeq databases were
        from July 2013


