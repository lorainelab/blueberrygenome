#!/usr/bin/env python

ex=\
"""
Read fasta file and report lengths of proteins.
"""
import sys,os,optparse,gzip
from Bio import SeqIO

def readSeq(file=None):
    if not file:
        handle = sys.stdin
    elif file.endswith('.gz'):
        handle = gzip.GzipFile(file)
    else:
        handle=open(file)
    record_dict=SeqIO.to_dict(SeqIO.parse(handle,'fasta'))
    return record_dict

def main(file=None,d=None):
    if not d:
        d = readSeq(file=file)
        sys.stderr.write("Read file: %s\n"%file)
    d2 = {}
    for (name,record) in d.items():
        toks = name.split('.')
        if len(toks)>1:
            gene_id='.'.join(toks[0:-1])
        else:
            gene_id = name
        sequence=record.seq
        if sequence.endswith('*'):
            size = len(sequence)-1
        else:
            size = len(sequence)
        if not d2.has_key(gene_id):
            d2[gene_id]={}
        d2[gene_id][name]=size
    toks=['gene','transcript','aa']
    sys.stdout.write('\t'.join(toks)+'\n')
    for (gene_id,subd) in d2.items():
        for (tx_id,size) in subd.items():
            toks=[gene_id,tx_id,str(size)]
            sys.stdout.write('\t'.join(toks)+'\n')
    return d2


           
if __name__ == '__main__':
    usage = "%prog -f file\n"+ex
    parser = optparse.OptionParser(usage)
    parser.add_option('-f','--file',dest='file',help='fasta file to read',
                       default=None)
    (options,args)=parser.parse_args()
    main(options.file)

