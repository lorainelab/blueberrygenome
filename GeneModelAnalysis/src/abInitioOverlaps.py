#!/usr/bin/env python

ex=\
"""
Read bed file containing merged predicted models and count the number of
Cufflinks models that overlap an ab initio gene prediction. This checks 
that the process of making non-redundant genes set worked as expected.
Uses the fact that CUFFlinks gene models have different naming convention.

Prints number to stdout.

ex)

   %prog bedtoolsMergedAllModels.bed

Requires output from running bedtools (2.20.0 or higher) merge with 
option to merge names:

bedtools merge -i FILE -c 4 -o
"""
import sys,optparse,fileinput

def main(fname=None):
    N=0
    M=0
    for line in fileinput.input(fname):
        names=line.rstrip().split('\t')[3]
        names=names.split(';')
        if len(names)>1:
            cuffs=filter(lambda x:x.startswith('CUFF'),names)
            if len(cuffs)<len(names):
                N=N+1
                M=M+len(names)-len(cuffs)
    sys.stdout.write('%i regions with Cufflinks and ab initio models\n'%N)
    sys.stdout.write('%i ab initio models overlap with a Cufflinks model\n'%M)

if __name__ == '__main__':
    usage = "%prog [file ...]\n"+ex
    parser = optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args)

