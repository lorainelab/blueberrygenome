#!/bin/bash

## use mergeBed to collect gene models that overlap into
## gene regions
## depends on bedtools v. 2.20.0 or higher 
GENES=data/V_corymbosum_scaffold_May_2013.bed.gz 
MERGED=results/merged.bed
gunzip -c $GENES | cut -f1-12 | sort -k1,1 -k2,2n | bedtools merge -i stdin -s -c 4 -o collapse > $MERGED 
gzip -f $MERGED
# -s only merge features that are on the same strand
# -c 4 -o collapse names of merged features 

