#!/usr/bin/env python

ex=\
"""
Make a file size (bp) of the largest spliced transcript per gene.
Needed for calculating RPKM.
Input: Bed detail file with gene id in field 13
Output: tab-delimited text, gene id is field 1, size is field 2.

example)

  gunzip -c data/V_corymbosum_scaffold_May_2013.bed.gz | makeTxSize.py | gzip -c > results/tx_size.txt.gz
"""

import optparse,sys,fileinput

def main(fn):
    d={}
    for line in fileinput.input(args):
        toks=line.rstrip().split('\t')
        if not len(toks)==14:
            raise ValueError("Not a BED Detail file. I quit.")
        gene_id=toks[12]
        # http://genome.ucsc.edu/FAQ/FAQformat.html#format1
        # 11th field is blockSizes
        esizes=toks[10]
        if esizes.endswith(','):
            esizes=esizes[0:-1]
        tx_size=sum(map(int,esizes.split(',')))
        if not d.has_key(gene_id):
            d[gene_id]=tx_size
        elif d[gene_id]<tx_size:
            d[gene_id]=tx_size
    sys.stdout.write("locus\tbp\n")
    for (gene_id,tx_size) in d.items():
        sys.stdout.write('%s\t%i\n'%(gene_id,tx_size))

if __name__ == '__main__':
    usage = "%prog [file ...]\n"+ex
    parser = optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args)
