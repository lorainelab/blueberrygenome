#!/usr/bin/env python

import re,sys,fileinput
"""
Find examples of transcripts with different gene ids that mergeBed merged
because they overlapped on the same strand.

Run this on the output of mergeModels.sh, like so:

   findWronglyMerged.py data/merged.bed > wrongmerged.bed

Then open in IGB to view the carnage.
"""

def main():
    for line in fileinput.input():
        (seq,start,end,names,strand)=line.rstrip().split('\t')
        names=names.split(';')
        nametoks=names[0].split('.')
        prefix='.'.join(nametoks[0:len(nametoks)-1])
        for name in names:
            if not name.startswith(prefix):
                line='\t'.join([seq,start,end,';'.join(names),'0',strand])+'\n'
                sys.stdout.write(line)
                break

if __name__ == '__main__':
    main()
