What's here:

dev_series_counts.csv.gz - Comma-separated file containing concatenated output from countReadsWithSamTools.py. Needs to be reformatted and processed to generate counts table that can be used in differential expression analysis.

alignments_distribution.txt.gz - Lists number of alignments for reads that mapped once, twice, three times, etc. 

