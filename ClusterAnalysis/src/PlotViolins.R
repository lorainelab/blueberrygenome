plotViolins=function(col=NULL,samples=NULL,main=NULL,
                     results=NULL,ylab=NULL) {
  par(las=1)
  if (is.null(main)) {main="Violin Plot"}
  if (is.null(ylab)) {ylab="Scaled RPKM"}
  # set up frame, without axes
  plot(1,1,xlim=c(0.5,length(samples)+0.5),
       ylim=c(-2,2),type="n",
       xlab="",ylab="Scaled RPKM",axes=FALSE,
       main='Cluster 2')
  ## bottom axis, with labels
  axis(side=1,at=1:length(samples),labels=samples)
  axis(side=2)
  for (i in 1:length(samples)) {
    sample=samples[i]
    vioplot(results[,sample],at=i,col=col[i],add=TRUE)
  }
}