Cluster 2 from STEM
=========================

Introduction
-------------

I used the STEM software to group DE genes into clusters. Three of the genes I'd already picked out as having interesting expression profiles turned out to be from  clusters 1, 6, and 0. But STEM also found a profile (number 2) that I had not really noticed in the expression data prior to running STEM. So now I'm going to dig a little deeper into the results to find out what types of genes are showing up in that profile. And also find an example for a figure.

Question:

* Can I notice any interesting trends with respect to functions for genes that low in MG and high in the other stages - the Cluster 2 profile.

### About the input file

```{r echo=FALSE}
f='results/StemResults10Clusters.tsv.gz'
```

To start, I made a file that included just the genes that were found to be DE in at least one comparison. Gene expression values were scaled so that the average value was zero across all samples and values were expressed in standard deviations above and below the mean value. This was done a gene by gene basis. Next, I opened the file in STEm and selected option "no normalization", 10 clusters, and any number of missing values were allowed. (No values were missing so probably this was not necessary.) Then I used the STEM Clustering Method to make clusters. The results from that are saved in `r f`. 

Analysis
--------

### Read data

Read STEM output file `r f` and fix format:

```{r}
d=read.delim(f,skip=1,header=T,as.is=T)
d=d[,2:ncol(d)]
row.names(d)=d$gene
two=d[d$Profile==2,]
```

Get gene annotations:

```{r}
f='../PlantCyc/results/V_corymbosum_scaffold_May_2013_wDescrPwy.bed'
annots=read.delim(f,header=F,as.is=T,quote='',sep='\t')[,c(13,14)]
names(annots)=c('gene','descr')
annots=annots[!duplicated(annots$gene),]
results=merge(two,annots,by.x='gene',by.y='gene')
row.names(results)=results$gene
```

### Visualize trends

View expression distribution by stage for profile 2:

```{r fig.width=5,fig.height=5}
library(vioplot)
samples=c('pad','cup','mg','pink','ripe')
source('../src/Vars.R')
source('src/PlotViolins.R')
col=unique(getColors())
plotViolins(results=results,samples=samples,
            col=col,main="Profile Two")
```

The violin plots show a trend of high, then low, then even higher, similar to what STEM found for these genes. But why are the low and high ends cut off? Maybe STEM put those genes into a different cluster?

### View gene information

Get rid of columns we don't need:

```{r}
results=results[,c(samples,'descr')]
```

Get genes annotated to pathways:

```{r}
indexes=grep('PWY',results$descr)
res=results[indexes,]
```

Reviewing the list, I'm surprised to see cytokinin-related genes increasing in fruit. Other genes that are high in ripe berries are not as surprising - flavnonoid biosynthesis genes, and so on. 

```{r echo=FALSE}
g='CUFF.9499'
```

For the differential expression figure, I picked `r g`:

```{r}
res[g,]
```

Conclusions
-----------

I didn't see any functional trends, but all I did was look at annotations for genes in Cluster 2. GO analsis might be more useful.

Session Info
------------

```{r}
sessionInfo()
```
