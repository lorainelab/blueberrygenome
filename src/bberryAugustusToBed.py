#!/usr/bin/env python

"Read Augustus GFF file and write out as BED14 (BED detail) format."

import os,sys,re,optparse
import Mapping.Parser.Bed as Bed
import Mapping.Parser.Gff3 as p
import gff3ToBedDetail as g
import Mapping.FeatureModel as feature

def readGenomeDescription(fname="genome.txt"):
    fh = open(fname)
    lines = fh.readlines()
    fh.close()
    d={}
    for line in lines:
        (chr,size)=line.rstrip().split('\t')
        d[chr]=size
    return d

def readGffFile(fname=None,seqs=None):
    models=g.readGffFile(fname=fname)
    newmodels=[]
    for model in models:
        seqname=model.getSeqname().split('|')[0]
        if seqs.has_key(seqname):
            newmodels.append(model)
            model.setSeqname(seqname)
    # augustus GFF does not have exons
    for model in newmodels:
        for feat in model.getFeats(feat_type='CDS'):
            newfeat=feature.DNASeqFeature(seqname=feat.getSeqname(),
                                          start=feat.getStart(),
                                          length=feat.getLength(),
                                          strand=model.getStrand(),
                                          feat_type='exon')
            model.addFeat(newfeat)
    return newmodels

def writeBedFile(fname=None,feats=None):
    Bed.feats2bed(feats,fname=fname,bed_format='BED12')

def convert(bed_file=None,
            gff_file=None,
            genome_descr=None):
    seqs = readGenomeDescription(fname=genome_descr)
    feats = readGffFile(fname=gff_file,seqs=seqs)
    writeBedFile(fname=bed_file,feats=feats)

def main(bed_file=None,gff_file=None,genome_descr=None):
    convert(bed_file=bed_file,
            gff_file=gff_file,
            genome_descr=genome_descr)

if __name__ == '__main__':
    usage = "%prog [options]"
    parser = optparse.OptionParser(usage)
    parser.add_option("-g","--gff_file",help="GFF3 file to convert. Can be compressed. [required]",dest="gff_file"),
    parser.add_option("-b","--bed_file",help="BED14 (bed detail) format file to write [required]",
                      dest="bed_file",default=None)
    parser.add_option("-G","--genome",help="Genome description file (genome.txt) containing annotated sequence names [required]",
                        dest="genome_descr",default="genome.txt")
    (options,args)=parser.parse_args()
    if not options.bed_file or not options.gff_file:
        parser.error("Bed or GFF files not specified.")
    if not options.genome_descr:
        parser.error("Genome description not specified.")
    main(gff_file=options.gff_file,
         bed_file=options.bed_file,
         genome_descr=options.genome_descr)
