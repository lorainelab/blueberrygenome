# project wide variables

# getColors=function() {
#   colors=c(rep("#00E41A",3), # pad
#            rep("#00C935",3), # cup
#            rep("#00A15D",3), # "#00A15D"
#            rep('#EF597B',3),  # French rose
#            rep("#0043BB",3)) # # blue
#   return(colors)
# }
# function replaced on 1 June 2016

getColors=function(samples=getSamples()) {
  d=read.delim("../src/SampleVariables.txt", stringsAsFactors = F)
  row.names(d) = d[,1]
  d=d[samples,]
  colors=d$Color
  names(colors)=d$Sample.ID
  return(colors)
}

getSamples=function(){
  d=read.delim("../src/SampleVariables.txt", stringsAsFactors = F)
  samples=d[,"Sample.ID"]
  return(samples)
}

getSraSampleNames = function(samples=getSamples()){
  d=read.delim("../src/SampleVariables.txt", stringsAsFactors = F)
  row.names(d) = d[,1]
  d=d[samples,]
  srr=d$SRA.ID
  names(srr)=d$Sample.ID
  return(srr)
}