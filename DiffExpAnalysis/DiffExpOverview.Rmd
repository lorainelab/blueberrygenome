Gene Ontology Enrichment Analysis of Pads versus Cups GO comparison
========================================================

Introduction
------------

Question we aim to answer include:

* Which categories are unusually enriched with DE genes overall?
* How many genes were DE in at least one comparison? 


Analysis
--------

Pick an alpha for differential expression and GO term significance:

```{r}
FDR=0.001
go_apha=0.1
```

### GO terms

Read GO annotation file for blueberry:

```{r}
gene2go='../ManuscriptsSupplementalDataFiles/SupplementalFiguresTables/S4-GO/S4-GO.tsv'
gene2go=read.delim(gene2go,sep='\t',header=T)[,c('gene','id')]
names(gene2go)=c('gene','category')
```

### Differential expression results

Read results from differential expression analysis files, create a list object with results for each comparison.

```{r}
fnames=c('padVcup','padVgreen','padVpink','padVripe','cupVgreen','cupVpink','cupVripe','greenVpink','greenVripe','pinkVripe')
biglist=list()
results=NULL
for (fname in fnames) {
  tmp=read.delim(paste0('results/',fname,'.tsv.gz'),
                 sep='\t',header=T,as.is=T)[,c('gene','padj','logFC','descr')]
  tmp=tmp[tmp$padj<=FDR,]
  biglist[[fname]]=tmp
  if (is.null(results)) {
    results=tmp
  }
  else { 
    results=rbind(results,tmp)
  }
}
o=order(results$padj)
results=results[o,]
results=results[!duplicated(results$gene),]
```

### Gene Ontology terms

Read GO term definitions:

```{r}
fname='../ExternalDataSets/go_defs.tsv.gz'
go_defs=read.delim(fname,sep='\t',header=T,as.is=T)[,c('id','name')]
names(go_defs)=c('GO_id','name')
```

Load GOSeq library:

```{r}
suppressPackageStartupMessages(library(goseq))
```

Define a function for the GO term analysis:

```{r}
do.go=function(de_results,length.vector,de_alpha,gene2go,go_defs,fname,
               go_alpha){
  gene.vector=rep(0,nrow(de_results))
  de=which(de_results$padj<=de_alpha)
  gene.vector[de]=1
  names(gene.vector)=row.names(de_results)
  pwf<-nullp(gene.vector,bias.data=length.vector[names(gene.vector)])
  GO=goseq(pwf,gene2cat=gene2go,method="Wallenius")
  GO=merge(GO,go_defs,by.x="category",by.y="GO_id",all.x=T)
  names(GO)[2]='o'
  names(GO)[3]='u'
  GO$o=p.adjust(GO$o,method='BH')
  GO$u=p.adjust(GO$u,method='BH')
  o=order(GO$o,decreasing=F)
  GO=GO[o,]
  v=union(which(GO$o<=go_alpha),which(GO$u<=go_alpha))
  GO=GO[v,]
  if (length(v)>0) {
    write.table(GO,file=fname,row.names=F,quote=T,sep="\t")
  }
  GO
}
```

### GO term enrichment analysis

#### Cups versus pads

```{r}
GO=do.go(results,sizes,FDR,gene2go,go_defs,'cupVpadGO.tsv',go_alpha)
GO
```

#### Green versus pink

```{r}
fname='results/greenVpink.tsv.gz'
results=read.delim(fname,sep='\t',header=T)
row.names(results)=results$gene
results=results[,c('logFC','padj','descr')]
GO=do.go(results,sizes,FDR,gene2go,go_defs,'greenVpinkGO.tsv',go_alpha)
GO
```




