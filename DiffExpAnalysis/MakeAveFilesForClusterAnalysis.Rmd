Creating expression average files for DE genes only
===========================

Introduction
------------

The goal of this file is to create files containing averaged and scaled average expression data for genes that were DE in at least one comparison between time points. This is to enable visualization of clustered gene expression profiles using the STEM software. 

For details about STEM, see:

* [STEM: a tool for the analysis of short time series gene expression data.](http://www.ncbi.nlm.nih.gov/pubmed/16597342)

Methods
-------

Average RPKM and scaled RPKM were calculated in MakeAveFiles.Rmd. 

Differential expression was analyzed in DeAllVsAll.Rmd.

Load averaged RPKM and scaled RPKM data:

```{r}
fname='results/aveRPKM.tsv.gz'
rpkm=read.delim(fname,header=T,sep='\t',quote="",as.is=T)
row.names(rpkm)=rpkm$gene
fname='results/scaledAveRPKM.tsv.gz'
scaled.rpkm=read.delim(fname,header=T,
                       sep='\t',quote="",as.is=T)
row.names(scaled.rpkm)=scaled.rpkm$gene
```

Load expression analysis results and get DE genes:

```{r}
fdr=0.00001
files=dir('results')
resultsfiles=files[grep('V',files)]
results=NULL
for (fname in resultsfiles) {
  fname=file.path('results',fname)
  d=read.delim(fname,header=T,sep='\t',quote="",as.is=T)
  if (is.null(results)) {
    results=d[d$padj<=fdr,c('gene','padj')]
  }
  else {
    results=rbind(results,d[d$padj<=fdr,c('gene','padj')])
  }
}
genes=results[!duplicated(results$gene),]$gene
```

Using fdr `r fdr` there were `r length(genes)` DE genes.

Get the subset of genes that are DE from the RPKM and scaled RPKM data frames:

```{r}
scaled.rpkm.reduced=scaled.rpkm[genes,]
rpkm.reduced=rpkm[genes,]
```

Write the data to a file so we can use it with STEM software:

```{r}
fname='results/scaledAveRPKM-DE.tsv'
write.table(scaled.rpkm.reduced,file=fname,quote=F,
            row.names=F,sep='\t')
fname='results/aveRPKM-DE.tsv'
write.table(rpkm.reduced,file=fname,quote=F,
            row.names=F,sep='\t')
```

Conclusion
==========

We wrote scaled and scaled averages RPKM data for genes that were DE at fdr `r fdr`. There were `r length(genes)` that met this criteria in at least one comparison

Limitations
===========

The choice of `r fdr` was somewhat arbitrary, but seems sufficiently stringent to eliminate false positives.

Session information
===================

```{r}
sessionInfo()
````

