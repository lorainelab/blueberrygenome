Gene Expression Profiling during berry fruit development and ripening
=====================================================================

Introduction
------------
The goal of this analysis is to identify which genes, if any, are up or down-regulated during berry fruit development and ripening.

Questions include:

* How many and which genes are up in ripe fruit but down in earlier stage fruit?
* How many and which genes are up in early stage fruit and down in later stage fruit?

Also, in a run of Cufflinks, Vikas found that the MG to Pink transition had the largest number of differentially expressed genes for any adjacent pair of samples in the time course. Here, we'll find if if the results from EdgeR agree.

Structure of Experiment
-----------------------

The experiments include five stages of berry fruit development and ripening including:

* pads (earliest stage)
* cups (next oldest)
* green mature (fully round, firm green fruit)
* pink (mostly green but with a blush of pink)
* ripe (big, blue, juicy berries)

For each stage, April collected berries from one of three plants, except for the pads stage, where she collected samples from only two plants.

However, we sequenced one 
of the pad libraries (from plant 3-33) twice on two different flow cells/lanes, thus providing (kind of)
a total of three replicates for the pads sample type. 

Note that in the previous step, we relabeled plants as:

* 2-41 is P1
* 2-42 is P2
* 3-33 is P3

Analysis
--------

Read counts per gene, per sample:

```{r}
fname='../CountsData/results/berry_dev.tsv.gz'
d=read.delim(fname,header=T,sep='\t')
rownames(d)=d$gene
```

For annotations, use field 14 from the BED14 file containing gene annotations.

**Note** Because PlantCyc included quotes in some of its descriptions and enzyme names, tell R to ignore quote characters.

```{r}
fname='../PlantCyc/results/V_corymbosum_scaffold_May_2013_wDescrPwy.bed'
annots=read.delim(fname,header=F,sep='\t',as.is=T,quote="")
annots=annots[,13:14]
names(annots)=c('gene','descr')
annots=annots[!duplicated(annots$gene),]
d=merge(d,annots,by.x="gene",by.y="gene",all.x=T)
annots=d[,c('gene','descr')]
row.names(annots)=annots$gene
```

Create objects need to run the edgeR analysis.

```{r message=FALSE,warning=FALSE}
suppressPackageStartupMessages(library(edgeR))
pad.indexes=grep('pad',names(d))
cup.indexes=grep('cup',names(d))
mg.indexes=grep('green',names(d))
pink.indexes=grep('pink',names(d))
ripe.indexes=grep('ripe',names(d))
indexes=c(pad.indexes,
          cup.indexes,
          mg.indexes,
          pink.indexes,
          ripe.indexes)
counts=d[,indexes]
rownames(counts)=d$gene
group=character()
group=append(group,rep('pad',length(pad.indexes)))
group=append(group,rep('cup',length(cup.indexes)))
group=append(group,rep('green',length(mg.indexes)))
group=append(group,rep('pink',length(pink.indexes)))
group=append(group,rep('ripe',length(ripe.indexes)))
cds=DGEList(counts,group=group,genes=annots)
```

Remove rows with zero counts. 


```{r}
zeros=apply(cds$counts,1,sum)==0
allzeros=sum(zeros)
cds=cds[!zeros,]
```

There were `r allzeros` genes with zero counts in all `r ncol(counts)` samples.

Normalize by library size, where library size is calculated from the number of reads overlapping the genes. 

```{r}
cds = calcNormFactors(cds)
```


Sample clustering
-----------------

Use multi-dimensional scaling to view relationships between samples:

```{r fig.height=6,fig.width=6}
colnames=colnames(cds$counts)
pad=grep('pad',colnames)
cup=grep('cup',colnames)
mg=grep('green',colnames)
pink=grep('pink',colnames)
ripe=grep('ripe',colnames)
library(RColorBrewer)
pal=colorRampPalette(c("green","blue"))
cols=pal(n=20)
colors=c(rep(cols[3],length(pad)), # "#00E41A"
        rep(cols[5],length(cup)), # "#00C935"
        rep(cols[8],length(mg)), # "#00A15D"
        rep('#EF597B',length(pink)),  # French rose
        rep(cols[15],length(ripe))) # "#0043BB"
main='MDS Plot'
par(las=1)
plotMDS(cds,main=main,labels=colnames,col=colors)
pth='../ManuscriptsSupplementalDataFiles/Figures'
f=file.path(pth,grep('Differential',dir(pth),value=T),'MDS.tiff')
quartz(file=f,width=5,height=5,dpi=600,type="tiff")
par(las=1)
plotMDS(cds,main=main,labels=colnames,col=colors)
dev.off()
```

The samples are clustered by stage, showing that the main source of variation is sample type rather than some other factor.

Hierarchial clustering by sample type
-------------------------------------

Now let's use hierachical clustering to examine similarities between samples:

```{r fig.width=6,fig.height=7}
suppressPackageStartupMessages(library(genefilter))
normalized.counts=cpm(cds)
newcounts=log2(normalized.counts)
for (i in 1:ncol(newcounts)) {
  v=newcounts[,i]
  indexes=which(v==-Inf)
  v[indexes]=0
  newcounts[,i]=v
}
dd=dist2(newcounts)
dd.row=as.dendrogram(hclust(as.dist(dd)))
row.ord=order.dendrogram(dd.row)
suppressPackageStartupMessages(library(latticeExtra))
legend=list(top=list(fun=dendrogramGrob,
                     args=list(x=dd.row,side="top" )))
levelplot(dd[row.ord,row.ord],
          scales=list(x=list(rot=90),cex=1.3),xlab="",
          ylab="",legend=legend,
          colorkey=list(labels=list(cex=1.3)))
```

```{r fig.width=6,fig.height=6}
transposed=t(normalized.counts) # transposes the counts matrix
distance=dist(transposed) # calculates distance
clusters=hclust(distance) # does hierarchical clustering
plot(clusters) # plots the clusters as a dendrogram
```


Estimating variance
-------------------

Identifying differentially expressed genes requires estimating sample variance as accurately as possible. 

In edgeR, this involves estimating common and tagwise dispersion for each gene.

* Estimate the common dispersion:

```{r}
cds = estimateCommonDisp(cds)
cds$common.dispersion # the estimate
```

* Estimate tagwise dispersion:

```{r}
prior.df=50/(ncol(counts)-length(unique(group)))
cds=estimateTagwiseDisp(cds,prior.df=prior.df)
summary(cds$tagwise.dispersion)
```

Visualize the relationship between mean expression and variance:

```{r fig.height=5,fig.width=7,warning=FALSE,message=FALSE}
plotMeanVar(cds,
  show.raw.vars=T,
  show.tagwise.vars=T,
  NBline=T,
  show.binned.common.disp.vars=F,
  show.ave.raw.vars=F,
  dispersion.method="qcml",
  nbins=100,pch=16,
  xlab="Mean Expression (Log10 Scale)",
  ylab="Variance (Log10 Scale)",main="Mean-Variance Plot")
```


Every pairwise comparison
=========================

Build a matrix of differential expression counts comparing every sample to every other sample. 

Each cell compares two stages - a later stage and an earlier stage.

The upper triangle reports the number of genes that were up-regulated in the later stage for the two stages being compared.

The lower triangle reports the number of genes that were down-regulated in the later stage for the two stages being compared.


```{r}
resultsdir="results"
alpha=0.001
labels=unique(group)
N=length(labels)
m=matrix(rep(NA,N**2),nrow=N)
row.names(m)=labels
colnames(m)=labels
i = 0
for (label in labels[1:N-1]) {
  i = i + 1
  for (j in seq(i+1,N)) {
    label2=labels[j]
    # second label is later treatment
    # up means: goes up with time
    dex=exactTest(cds,dispersion="tagwise",pair=c(label,label2))
    dex$table$PValue=p.adjust(dex$table$PValue,method='BH') 
    o=order(dex$table$PValue)
    n.up=sum(dex$table$PValue<=alpha & dex$table$logFC > 0)
    n.dn=sum(dex$table$PValue<=alpha & dex$table$logFC < 0)
    m[label,label2]=n.up
    m[label2,label]=n.dn
    fname=file.path(resultsdir,paste0(label,'V',label2,'.tsv'))
    dex=dex[o,]
    cpms=cpm(cds$counts[row.names(dex$table),
                        c(grep(label,colnames(counts)),
                          grep(label2,colnames(counts)))])
    res=data.frame(gene=dex$genes$gene,
                   descr=dex$genes$descr,
                   logFC=dex$table$logFC,
                   padj=dex$table$PValue,
                   cpms)
    write.table(res,file=fname,row.names=F,sep='\t',quote=F)
    system(paste('gzip -f',fname))
  }
}
m
```

The matrix of DE gene counts shows there are not a lot of genes that are different (either up or down) between pink and ripe fruit. However, there are more genes that are different between pad and mature green than between pad and cup. Similarly, even more genes are different between pad and pink. However, about the same number (slightly less in fact) are different between pad and ripe. And when we look at pink versus ripe, we find only a few genes are different at `r alpha`.

The biggest adjacent transition
-------------------------------

A run of CuffDiff done by Vikas showed the MG to pink transition had the highest number of DE genes.

Do the EdgeR results agree?

```{r}
# there are five stages and five minus one transitions 
m2=matrix(rep(NA,N**2),nrow=N)
colnames(m2)=labels
row.names(m2)=labels
i=1
for (i in seq(i,N-1)){
  label1=labels[i]
  label2=labels[i+1]
  n.de=m[label1,label2]+m[label2,label1]
  m2[label1,label2]=n.de
}
m2
```


Yes, it appears the edgeR results agree: the MG versus Pink transition has the most DE genes.

CONCLUSIONS
===========

Many genes are differentially expressed.

More distant sample types (in the time course) have the largest number of differences.

Of the adjacent stages, MG versus Pink are the most different.

Limitations of the Analysis
===========================

One limitations is that the pads sample type had only two bioogical replicates; recall that P3a_pad and P3b_pad were from the same library. As a result, the variance for this sample type may be underestimated.

Another limitation is that staging the berries was somewhat difficult. The pads and cups stages do not last long and so it samples collected at this stage were likely very close in age. However, ripe blueberries that are a couple of weeks apart in age look very much the same when picked. Older berries are sweeter, softer and perhaps more flavorful, but since we were collected berries for RNA extraction and needed to freeze them on liquid nitrogen immediately after picking them, we couldn't easily perform tests to determine the age of the fruit. W

A third limitations is that the gene models were based on an incomplete, draft genome, and so many of the gene models may be incomplete and their a
nnotations incorrect. We were able to get around some of these problems by basing our annotation on results from blastx searches against nr, which is possibly the most complete database of protein sequences currently available. Because blastx searches every frame of the query against the target database, we can still find 'hits' even when the gene model sequences contain insertions, deletions, or other errors that introduce frameshifts. Thus, although we did predict CDSs (when possible) for every gene model, we did not have to rely on these possibly incorrect CDSs for the annotation. 


Session info
============

```{r}
sessionInfo()
````

