Creating expression average files
===========================

Introduction
------------

The goal of this file is to create files containing averaged and scaled expression data. This is to enable visualization of expression changes scaled together.


Methods
-------

RPKM per gene was calculated in MakeRpmRpkmFiles.Rmd. 

Load RPKM data:

```{r}
fname='results/berry_RPKM.tsv.gz'
d=read.delim(fname,header=T,sep='\t',quote="")
row.names(d)=d$gene
pad.indexes=grep('pad',names(d))
cup.indexes=grep('cup',names(d))
mg.indexes=grep('green',names(d))
pink.indexes=grep('pink',names(d))
ripe.indexes=grep('ripe',names(d))
indexes=c(pad.indexes,
          cup.indexes,
          mg.indexes,
          pink.indexes,
          ripe.indexes)
d=d[,indexes]
```

There were expression measurements for `r nrow(d)` genes.

Eliminate genes with zero RPKM:

```{r}
sums=apply(d,1,sum)
zeros=which(sums==0)
d=d[-zeros,]
```

Following zero expression filtering, there were `r nrow(d)` gene measurements.

Make new data frame with just the averages:

```{r}
doStage=function(d,index=1,num.reps=3,func=mean){
  apply(d[,index:(index+num.reps-1)],1,func)
}
ave=data.frame('pad'=doStage(d,1),
               'cup'=doStage(d,4),
                'mg'=doStage(d,7),
                'pink'=doStage(d,10),
                'ripe'=doStage(d,13))
```

Write the data to disk:

```{r}
f='results/aveRPKM.tsv'
tmp=data.frame(gene=row.names(ave),ave)
write.table(tmp,file=f,row.names=F,sep='\t',quote=F)
system(paste('gzip -f',f))
rm(tmp)
```

Scale expression values for each gene:

```{r}
d.transpose=t(d) # scale works on columns not rows
d.transpose.scaled=scale(d.transpose)
names(d.transpose.scaled)=row.names(d)
d.scaled=t(d.transpose.scaled)
names(d.scaled)=names(d)
```

Write the scaled data to disk:

```{r}
f='results/scaledRPKM.tsv'
tmp=data.frame(gene=row.names(d.scaled),d.scaled)
write.table(tmp,file=f,row.names=F,sep='\t',quote=F)
system(paste('gzip -f',f))
rm(tmp)
```

Calculate scaled averages and write them to disk:

```{r}
ave.scaled=data.frame('pad'=doStage(d.scaled,1),
                'cup'=doStage(d.scaled,4),
                'mg'=doStage(d.scaled,7),
                'pink'=doStage(d.scaled,10),
                'ripe'=doStage(d.scaled,13))
f='results/scaledAveRPKM.tsv'
tmp=data.frame(gene=row.names(ave.scaled),ave.scaled)
write.table(tmp,file=f,row.names=F,sep='\t',quote=F)
system(paste('gzip -f',f))
rm(tmp)
```

Conclusion
==========

We wrote scaled and scaled averages RPKM data files needed for other analyses.

Limitations
===========

I'm not sure the scaling method is appropropriate to this data set because the expression values are skewed and variance increases with expression level.

Session information
===================

```{r}
sessionInfo()
````

