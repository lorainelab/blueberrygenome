Gene Ontology Enrichment Analysis of Differentially Expressed Genes
========================================================

Introduction
------------

Differential expression (DE) analysis identified many genes that were DE between stages. Comparisons between early and later stages identified many DE genes, but adjacent stage comparisons had fewer, except for the mature green stage.

Here, Gene Ontology enrichment analysis identifies which functions and processes many DE genes belong to.

Questions:

* Which comparisons have the most categories?
* Which categories are enriched among genes that are DE between mature green and pink fruit?

Analysis
--------

### GO terms

Read GO annotation file for blueberry:

```{r}
gene2go='../ManuscriptsSupplementalDataFiles/SupplementalDataFiles/S3-GO/S3-GO.tsv'
gene2go=read.delim(gene2go,sep='\t',header=T)[,c('gene','id')]
names(gene2go)=c('gene','category')
```

### Transcript sizes

Read transcript sizes for blueberry genes:

```{r}
fname='../GeneModelAnalysis/results/tx_size.txt.gz'
sizes=read.delim(fname,header=T,sep='\t')
sizes$kb=sizes$bp/1000
locus_names=sizes$locus
sizes=sizes$kb
names(sizes)=locus_names
rm(locus_names)
```

Pick FDR cutoffs for differential expression and GO term significance:

```{r}
de_FDR=0.001
go_FDR=0.01
```

Load GOSeq library:

```{r}
suppressPackageStartupMessages(library(goseq))
```

Define a function for the GO term analysis:

```{r warning=FALSE}
do.go=function(de_results=NULL,sizes=NULL,
               de_FDR=0.001,go_FDR=0.01,
               gene2go=NULL,go_defs=NULL,
               early=NULL,late=NULL){
  gene.vector=rep(0,nrow(de_results))
  de=which(de_results$padj<=de_FDR)
  gene.vector[de]=1
  names(gene.vector)=de_results$gene
  pwf<-nullp(gene.vector,plot.fit=F,bias.data=sizes[names(gene.vector)])
  GO=goseq(pwf,gene2cat=gene2go,method="Wallenius",use_genes_without_cat=TRUE)
  names(GO)[2]='FDR.over_represented'
  names(GO)[3]='FDR.under_represented'
  GO$o=p.adjust(GO$FDR.over_represented,method='BH')
  GO$u=p.adjust(GO$FDR.under_represented,method='BH')
  o=order(GO$o,decreasing=F)
  GO=GO[o,]
  v=union(which(GO$FDR.over_represented<=go_FDR),which(GO$FDR.under_represented<=go_FDR))
  GO=GO[v,]
  GO$early=rep(early,nrow(GO))
  GO$late=rep(late,nrow(GO))
  GO=GO[,c('early','late','category','ontology','term','FDR.over_represented','FDR.under_represented','numDEInCat','numInCat')]
  GO
}
```

### GO term enrichment analysis

Get enriched GO categories for all the comparisons:

```{r}
fnames=dir('results',pattern="V")
all=NULL
for (fname in fnames) {
  samples=strsplit(strsplit(fname,'\\.')[[1]][1],'V')[[1]]
  early=samples[1]
  late=samples[2]
  de_results=read.delim(file.path('results',fname),
                        sep='\t',header=T,quote="")
  row.names(de_results)=de_results$gene
  GO=do.go(de_results=de_results,
           sizes=sizes,
           de_FDR=de_FDR,go_FDR=go_FDR,
           gene2go=gene2go,
           early=early,late=late)
  all=rbind(all,GO)
}
# remove terms GOSeq didn't recognize
all=all[!is.na(all$term),]
```

Write results:

```{r}
fname='results/GO.tsv'
write.table(all,file=fname,sep='\t',row.names=F)
pth='../ManuscriptsSupplementalDataFiles/SupplementalDataFiles/S4-GO-DE'
fname=file.path(pth,'GO-DE.tsv')
write.table(all,file=fname,sep='\t',row.names=F)
system(paste('gzip -f',fname))
```

How many categories were over-represented in each comparison?

```{r}
comp=paste0(all$early,'-',all$late)
v=which(all$FDR.over_represented<=go_FDR)
comp=comp[v]
table(comp)
```

How many categories were enriched in adjacent comparisons?

```{r}
table(comp)[c('pad-cup','cup-green','green-pink','pink-ripe')]
```


### Compare mature green to pink fruit

```{r}
comps=which(all$early=='green'&all$late=='pink')
gvp=all[comps,]
o=order(gvp$FDR.over_represented)
gvp=gvp[o,]
gvp=gvp[,3:9]
fname='results/G-green-pink.tsv'
write.table(gvp,file=fname,row.names=F,sep='\t',quote=F)
```

Terms significantly over-represented between green and pink stages include: 

```{r}
unique(gvp[gvp$FDR.over_represented<=go_FDR,]$term)
```

Terms significantly under-represented between green and pink stages include: 

```{r}
unique(gvp[gvp$FDR.under_represented<=go_FDR,]$term)
```

Unusually many genes with photosynthetic functions are DE. Are the genes up or down in pink fruit in relation to green fruit? Maybe chloroplasts are being remodeled into something like chromoplasts of tomato? If yes, I bet some genes are up and others are down. If chloroplasts are simply being broken down (does that happen?) then probably expression for all the photosynthetic genes decreases in the transition from mature green to pink fruit.

Let's find out:

```{r}
de=read.delim('results/greenVpink.tsv.gz',sep='\t',header=T,quote="")
de=de[de$padj<=de_FDR,]
GOplusGenes=merge(gvp,gene2go[gene2go$gene%in%de$gene,],
                  by.x='category',by.y='category')
GOplusGenes=merge(GOplusGenes,
                  de[,c('gene','logFC','descr')],
                  by.x='gene',by.y='gene')
GOplusGenes=GOplusGenes[,c('category','term',
                           'numDEInCat','numInCat',
                           'FDR.over_represented',
                           'FDR.under_represented',
                           'gene','logFC','descr')]
o=order(GOplusGenes$FDR.over_represented)
GOplusGenes=GOplusGenes[o,]
photo=subset(GOplusGenes,GOplusGenes$term=="photosynthesis")
thyla=subset(GOplusGenes,GOplusGenes$term=='thylakoid')
```

Of `r photo$numInCat[1]` genes annotated to term `r photo$term[1]`, `r photo$numDEInCat[1]` were DE between green and pink fruit. Of the DE genes, `r sum(photo$logFC<0)` were down-regulated and `r sum(photo$logFC>0)` were up-regulated.

Of `r thyla$numInCat[1]` genes annotated to term `r thyla$term[1]`, `r thyla$numDEInCat[1]` were DE between green and pink fruit. Of the DE genes, `r sum(thyla$logFC<0)` were down-regulated and `r sum(thyla$logFC>0)` were up-regulated.

Every gene annotated to these terms is down-regulated in pink fruit - i.e., has negative log2 fold-change. 

Interestingly, many DE genes were annotated to categories related to stress. It seems strange that berries undergoing normal development, maturation, and ripening are expressing stress response genes. 

Let's review DE genes annotated as stress-related:

```{r}
stress=subset(GOplusGenes,GOplusGenes$term=="response to stress")
```

Of `r stress$numInCat[1]` genes annotated to term `r stress$term[1]`, `r stress$numDEInCat[1]` were DE between green and pink fruit. Of the DE genes, `r sum(stress$logFC<0)` were down-regulated and `r sum(stress$logFC>0)` were up-regulated.

I noticed several genes annotated as peroxidases:

```{r}
indexes=grep('peroxidase',stress$descr)
peri=stress[indexes,]
```

Of the `r nrow(stress)` stress-annotated DE genes, `r length(indexes)` encoded peroxidase enzymes, and several were annotated to betalin and ascorbic acid pathways. Based on this and the other annotated functions, I don't think the berries are undergoing a stress response. I think instead we're seeing genes for pathways that in some contexts help with stress adaptation.

Conclusion
==========

Answering the questions:

* Which comparisons have the most categories? (Maybe this is a dumb question?)

Green versus other stages had large numbers of categories, as did comparisons between early and late stages. 


* Which categories are enriched among genes that are DE between mature green and pink fruit?

Enriched categories included functions related to photosynthesis and metabolic functions.

Session information
==================

```{r}
sessionInfo()
```
