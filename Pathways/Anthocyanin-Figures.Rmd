Introduction
------------

This Markdown creates gene expression plots for anthocyanin biosynthetic enzymes.

Analysis
--------

Getting the pathway ids and pathway names. We'll examine all pathways in Pathways Class: Anthocyanins Biosynthesis:

http://pmn.plantcyc.org/PLANT/NEW-IMAGE?object=ANTHOCYANIN-SYN


```{r}
# instances
pwys=c('PWY-5125','PWY-5127','PWY-5139','PWY-6062','PWY-5313-ARA','PWY-5310','PWY-5312','PWY-5153','PWY-5286','PWY-5160','PWY-5268','PWY-5284','PWY-5295')

# names
pwy.names=c('anthocyanin biosynthesis (pelargonidin 3-O-glucoside, cyanidin 3-O-glucoside)','gentiodelphin biosynthesis','pelargonidin conjugates biosynthesis','peonidin and derivatives biosynthesis','superpathway of anthocyanin biosynthesis (from cyanidin and cyanidin 3-O-glucoside)','superpathway of anthocyanin biosynthesis (from delphinidin 3-O-glucoside)','superpathway of anthocyanin biosynthesis (from pelargonidin 3-O-glucoside)','anthocyanin biosynthesis (delphinidin 3-O-glucoside)','anthocyanidin sophoroside metabolism','rose anthocyanin biosynthesis','salvianin biosynthesis','shisonin biosynthesis','ternatin C5 biosynthesis')
```

Load functions:

```{r}
source('src/pathway_funcs.R')
```

Get all the genes:

```{r}
genes=getPathwayGenes(pwys)
ecs=getEC()[genes]
rpkm=getRPKM()
```

There were `r length(genes)` genes annotated to these pathways and `length(unique(ecs))` EC numbers. 

EC numbers and the number of genes for each was:

```{r}
table(ecs)
```

Plotting expression of genes by EC number.

```{r}
# anthocyanidin glucoside glucosyltransfersae
gg='EC-2.4.1'
gt='EC-2.4.1.115'
# leucocyanidin oxygenase
# http://www.brenda-enzymes.info/php/result_flat.php4?ecno=1.14.11.19&organism=
lo='EC-1.14.11.19'
```

View the genes and their annotatons:

```{r}
annots=read.delim('../PlantCyc/results/V_corymbosum_scaffold_May_2013_wDescrPwy.bed',
                  quote='',header=F,sep='\t')[,c(13,14)]
names(annots)=c('gene','descr')
annots=annots[!duplicated(annots$gene),]
row.names(annots)=annots$gene
```

Start with `r lo`:

```{r fig.width=5,fig.height=5}
library(RColorBrewer) # http://bl.ocks.org/mbostock/5577023
scheme="Dark2"
brew=brewer.pal(8,scheme)
main='leucocyanidin oxygenase'
labels=c("Pad","Cup","Green","Pink","Ripe")
EC=lo
gs=names(ecs[ecs==EC])
expr=rpkm[gs,]
showExprVals(expr,ecs[gs],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
# Make figure for publication:
folder='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Anthocyanin',files)],'/',EC,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=5,height=5,
       type="tiff")
showExprVals(expr,ecs[gs],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
dev.off()
```

View the gene annotations:

```{r}
annots[gs,]
```


Next, `r gt`:

```{r fig.width=5,fig.height=8}
# http://www.enzyme-database.org/query.php?ec=2.4.1.115
main='anthocyanidin 3-O-glucosyltransferase'
EC=gt
gs=names(ecs[ecs==EC])
#allgs=names(ecs[ecs==EC])
#highgs=c('CUFF.20951','CUFF.35538')
#othergs=allgs[allgs!=highgs]
#gs=highgs
expr=rpkm[gs,]
showExprVals(expr,ecs[gs],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
# Make figure for publication:
folder='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Anthocyanin',files)],'/',EC,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=5,height=5,
       type="tiff")
showExprVals(expr,ecs[gs],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
dev.off()
annots[gs,]
```


For completness, let's do the 21 genes:

```{r fig.width=10,fig.height=10}

par(mfrow=c(2,2))
EC=gg
allgs=names(ecs[ecs==EC])
gs1=c('CUFF.10410','CUFF.14082','CUFF.18507','CUFF.14083','CUFF.10411') #5
gs2=c('CUFF.18489','CUFF.18490','CUFF.14062','CUFF.54606','gene.g7444.t1','gene.g7445.t1') #5
othergs=allgs[!allgs%in%c(gs1,gs2)]
gs3=othergs[1:5]
gs4=othergs[6:10]
showExprVals(rpkm[gs1,],ecs[gs1],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
showExprVals(rpkm[gs2,],ecs[gs2],main=main,legend=T,
             plx='topright',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
showExprVals(rpkm[gs3,],ecs[gs2],main=main,legend=T,
             plx='topright',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
showExprVals(rpkm[gs4,],ecs[gs2],main=main,legend=T,
             plx='topright',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
par(mfrow=c(1,1))
# Make figure:
older='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Anthocyanin',files)],'/',EC,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=10,height=10,
       type="tiff")
par(mfrow=c(2,2))
showExprVals(rpkm[gs1,],ecs[gs1],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
showExprVals(rpkm[gs2,],ecs[gs2],main=main,legend=T,
             plx='topright',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
showExprVals(rpkm[gs3,],ecs[gs2],main=main,legend=T,
             plx='topright',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)
showExprVals(rpkm[gs4,],ecs[gs2],main=main,legend=T,
             plx='topright',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)',col=brew)

dev.off()
annots[allgs,]
```






