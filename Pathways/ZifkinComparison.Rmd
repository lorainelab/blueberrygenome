Expression of genes encoding bixon biosynthetic pathway enzymes 
========================================================

Introduction
------------

This Markdown file looks at expression patterns for genes corresponding to ESTs described in [Zifkin, et al (2012)](http://www.ncbi.nlm.nih.gov/pubmed/22086422) that encoded enzymes involved in two flavonoid biosynthesis pathway brances:

* pro-anthocyanidin (PA) biosynthesis, associated with defense and bitter-tasting
* anthocyanin (AN) biosynthesis, up-regulated during ripening, protective as well as appealing to animals

We aligned all blueberry ESTs against the genome and identified overlapping gene annotations. 

```{r}
ANR='CUFF.29797'
DFR='CUFF.50634'
UFGT='CUFF.20951'
MYBPA1=c('CUFF.51789','CUFF.14288')
F3H='CUFF.39752'
F35Ha='Gene.g10884.t1'
F35Hb='CUFF.51728'
F35Hc='CUFF.51711'
NCED='CUFF.30377'
```

However, for some genes, no EST accessions were reported. Therefore for some of the enzymes, we used results from the homology searches to identify putative candidates.

Enzymes identified by homology searches:

```{r}
# anthronilate synthase

#ANS=c()
# leucoanthocyanidin reductase
# gene name : CUFF.16780
# description : SIMILAR TO GN80-9600-MONOMER (GSVIVT01008238001) EC-1.17.1.3 leucoanthocyanidin reductase (scov=95,ident=67.4) PlantCyc PWY-6029|2,3-<i>trans</i>-flavanols biosynthesis;PWY-6035|2,3-<i>cis</i>-flavanols biosynthesis
#gene name : CUFF.362
#description : SIMILAR TO GN80-11698-MONOMER (GSVIVT01011958001) EC-1.17.1.3 leucoanthocyanidin reductase (scov=96.8,ident=71.5) PlantCyc PWY-6035|2,3-<i>cis</i>-flavanols biosynthesis;PWY-6029|2,3-<i>trans</i>-flavanols biosynthesis
#gene name : CUFF.48731
#description : SIMILAR TO GN7Y-36438-MONOMER (Potri.010G129800.3) EC-1.17.1.3 leucoanthocyanidin reductase (scov=87.6,ident=75.8) PlantCyc PWY-6029|2,3-<i>trans</i>-flavanols biosynthesis;PWY-6035|2,3-<i>cis</i>-flavanols biosynthesis
LAR=c('CUFF.16780','CUFF.362','CUFF.48731')
```

We also used homology searches to try to identify 
For a diagram of the pathway, see [Figure 1](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3252089/figure/fig1/)

Comparing to Figure 5
---------------------

[Figure 5](http://www.ncbi.nlm.nih.gov/pmc/articles/PMC3252089/figure/fig5/) plots transcript expression levels at different stages of fruit development and ripening. Two patterns were observed:

* a biphasic pattern in which expression was high in early, declined as the fruit grew, and then increased was the fruit began to mature and ripen. 
* an early-high pattern in which expression was high in the early stages, declined, and then was mostly undetectable at the later stages.

Genes with biphasic expression pattern:

* DFR (`r DFR`) - common to both pathways, catalyze production of leucoanthocyanidins
* ANS (no model identified as yet) - anthronilate syntases, converts leucoanthocanidins to anthocyanidins, which can be shuttled into both the AN and PA pathways
* MYBPA1 (`r MYBPA1`) 

Genes with the early-high expression pattern:

* ANR (`r ANR`) anthronilate reducatase, converts anthocyanidins to epicatechin, a PA precursor
* LAR (?) leucocyanidin reductase, converts leucoanthocyanidin to catechin, a PA precursor


Load data sets we need
---------------------------------------------
```{r}
source('src/pathway_funcs.R')
annots=getAnnots()
scaled.rpkm=getScaledRPKM()
rpkm=getRPKM()
```

Visualizing expression data
---------------------------

Pick genes to visualize:

There is 1 DFR, 2 MYBPA1, 1 ANR, and 3 LARs, so we'll need four colors:

```{r}
genes=c(DFR,MYBPA1,ANR,LAR)
library(RColorBrewer)
scheme="Dark2"
brew = brewer.pal(4,scheme)
colors=c(brew[1],rep(brew[2],2),brew[3],rep(brew[4],3))
```


Make scaled and unscaled expression plots, using color to represent isozymes.

Plot using unscaled (RPKM) expression values
--------------------------------------------

```{r fig.width=7,fig.height=7}
expr=rpkm[genes,]
labels=c("Pad","Cup","MG","Pink","Ripe")
main=paste("RPKM for enzymes")
showExprVals(expr,NULL,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=0.75,cols=colors,lwd=1)
makePic(file='images/ExprPA-Zifkin-fig5.tiff')
showExprVals(expr,NULL,main=main,legend=F,
             plx='topleft',labels=labels,
             leg.cex=0.75,cols=colors,lwd=3)
dev.off()
```

Plot using scaled (RPKM) expression values
------------------------------------------

The following plot is the same as the preceding one, except that scaled expression values are shown. The scaling method used was R's "scale" method, which scales expression values for each gene by subtracting the mean (for that gene across all samples) and then dividing by the standard deviation of the mean. Thus, units are in standard deviations above and below the mean. 

```{r fig.width=7,fig.height=7}
expr=scaled.rpkm[genes,]
main=paste("Scaled RPKM")
showExprVals(expr,NULL,main=main,legend=T,
             plx='topright',
             leg.cex=1,cols=colors)
```

View annotations text for the genes
-----------------------------------

The `r "annots"` data frame contains annotation text for each gene, including percent subject coverage and overall percent identity. The percent subject coverage is the percentage of the matched protein that was covered by the original alignment and the overall percent identity is a weighted average (weighted by length of the subject covered by the HSP) of the percent identity of each HSP.  

Annotations for genes:

```{r}
annots[genes]
```


Conclusions
-----------


Limitations of the analysis
---------------------------

Expression levels may not correlate with enzyme levels or product levels.

Session information
-------------------

```{r}
sessionInfo()
```
