Analysis of dhurrin pathway
===========================

Introduction
------------

```{r}
pwy.id='PWY-861'
pwy.name='Dhurrin'
```

Pathway analysis identified the `r pwy.name` biosynthesis ([`r pwy.id`](http://pmn.plantcyc.org/PLANT/new-image?object=`r pwy.id`)) pathway as being differentially expressed.

In this Markdown, we'll investigate this results in more depth.

We'll retrieve pathway genes from other sources external to PlantCyc and use them to identify other blueberry genes likely to encode enyzmes in this pathway.

Dhurrin is a cyanogenic glycoside (CG) that releases hydrogen cyanide (HCN) when broken down by glycosidases. It's extremely abundant in sorghum seedlings and the first biosynthetic enzymes for dhurrin were identified in sorghum. Enzymes in the biosynthetic pathway include:

* CYP791A - substrate is tyrosine, produces 4-hydroxyphenylacetaldeoxime 
* CYP71E1 - converts 4-hydroxyphenylacetaldeoxime to 4-hydroxymandelonitrile (aglycone)
* UGT85B1 - attaches glucose to aglycone, producing dhurrin

Catabolic enzymes include:

* dhurrinase - removes sugar group, generating unstable aglycone, release HCN
* hydroxynitrile-lyase - converts aglycone (4-hydroxymandelonitrile) to HCN and 4-hydroxybensaldehyde 
* nitrilase 4 - detoxifies dhurrin, breaks it down into a non-cyanogenic compound

The dhurrin biosynthetic pathway probably does not exist in blueberry in the same form, but existence of enzymes similar to CG biosynthetic enzymes suggest blueberry produces a CG that plays a role in defense during berry development.

To identify other putative homologs, I collected sorghum and Arabidopsis sequences (see dhurrin.fa in "Dhurrin" folder) and used them to search a database of blueberry proteins. The results of the search are in Dhurrin/dhurrin_blastp.out. 

In this markdown, I'll:

* Examine results of the blast search and answer the question: Does blueberry contain candidates CG biosynthetic and catabolic enzymes?
* Visualize expression patterns for genes likely to encode CG enzyme homologs and answer the question: How are they expressed and when are CGs likely to be produced in blueberry fruit development and ripening?

Analysis
--------

For this analysis, we'll first look at genes identified by the PlantCyc-based annotation. Next, we'll examine the blast results, which retrieved more 

Identify genes in the pathway, according to the PlantCyc results:


```{r fig.width=5,fig.height=5}
fname='../PlantCyc/results/locus2pathway_60_45.tsv.gz'
source('src/pathway_funcs.R')
genes=getPathwayGenes(pwy.id,file=fname)
```

There were `r length(genes)` genes annotated to this pathway by the PlantCyc-based annotation.

How did we annotate these genes?

```{r}
annots=getAnnots()
annots[genes]
```

The annotations indicate that these two genes are putative homologs of CYP71E1, which converts 4-hydroxyphenylacetaldeoxime to 4-hydroxymandelonitrile (aglycone).

Subject coverage and overall percent identity are high. Note that in this case, subject coverage refers to the PlantCyc enzyme as the PlantCyc annotation used the blueberry cDNAs as queries against a database of PlantCyc enzymes.


Get expression data for the pathway - in scaled RPKM values so that we can compare patterns of increase or decrease without being distracted by overall expression level:

```{r}
scaled.rpkm=getScaledRPKM()
expr=scaled.rpkm[genes,]
ec=getEC()[genes]
```

Make scaled expression plots:

```{r}
library(RColorBrewer)
scheme="Dark2"
brew = brewer.pal(6,scheme)
labels=c("Pad","Cup","Green","Pink","Ripe")
main=paste("Scaled expression for",
           pwy.name,"enzymes")
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,cols=brew)
```

Note how the scaled expression patterns of the two dhurrin biosynthetic genes are extremely similar. In IGB, the two genes are side-by-side.

Now, examine unscaled absolute expression levels using the same type of plot:

```{r fig.height=5,fig.width=5}
rpkm=getRPKM()
expr=rpkm[genes,]
main=paste("RPKM expression values for",
           pwy.name,"enzymes")
showExprVals(expr,ec,main=main,plx='topleft',
             labels=labels,leg.cex=1,cols=brew)
```

Make high-quality images:

```{r}
folder='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Dhurrin',files)],
            '/',pwy.id,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=5,height=5,
       type="tiff")
main=paste(pwy.name,"(RPKM)")
expr=rpkm[genes,]
showExprVals(expr,ec,main=main,plx='topleft',
             leg.cex=1,cols=brew,labels=labels,
             legend=T)
dev.off()
```


Based on this, it appears that these two genes are up-regulated in the mature green (Gr) stage, when the berries are full-sized but haven't yet begun to turn color.

These results suggest that green berries contain dhurrin and the levels of dhurrin increase as berries grow and develop. However, once ripening begins, expression of the genes starts to fall and ultimately becomes nearly zero in ripe fruit. This suggests that dhurrin may be playing a role in blueberry as a defense compound that protects berries from herbivory.

Blastp results
=========

I made a fasta file with sorghum and Arabidopsis dhurrin genes and searched them against the blueberry proteins. Headers from the file were:

```{r}
system('grep ">" Dhurrin/dhurrin.fa')
```

Read blastp results:

```{r}
f='Dhurrin/dhurrin_blastp.out'
blast=read.delim(f,header=F,comment.char='#',as.is=T)
names(blast)=c('qid','subj','identity','qlen','qstart','qend','qcov','slen','sstart','send','evalue')
blast=blast[blast$identity>=45,]
blast=blast[blast$qcov>=60,]
f='../GeneModelAnalysis/data/V_corymbosum_scaffold_May_2013.bed.gz'
bed=read.delim(f,header=F,as.is=T)[,c(4,13)]
names(bed)=c('txid','locus')
blast=merge(blast,bed,by.x='subj',by.y='txid')
blast=blast[order(blast$identity,decreasing=T),]
blast=blast[!duplicated(blast$locus),]
expr=rpkm[blast$locus,]
expr=expr[!is.na(expr[,1]),]
samples=names(expr)
expr$locus=row.names(expr)
expr=merge(blast,expr,by.x='locus',by.y='locus')
expr=expr[order(expr$qid),c('locus','qid','identity','qcov',samples)]

```

Plot expression:

Query ids for biosynthetic pathway: CYP791A, CYP71E1, UGT85B1 
Query ids for cyanogenic, catabolic enzymes: dhurrinase1-1, dhurrinase1-2, hydroxynitrile-lyase
Query id for detoxifying enyzme: AtNIT4

```{r fig.width=8,fig.height=10}
folder='../ManuscriptsSupplementalDataFiles/Figures'
subfolder=grep('Dhurrin',dir(folder),value=T)
folder=file.path(folder,subfolder)
par(mfrow=c(3,2))
e='CYP791A'
genes=expr[expr$qid==e,]$locus
main=paste0(e,"-like genes")
showExprVals(rpkm[genes,],NULL,main=main,plx='topright',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
file=file.path(folder,paste0(e,'.tif'))
quartz(file=file,dpi=600,width=6,height=6,type="tiff")
showExprVals(rpkm[genes,],NULL,main=main,plx='topright',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
dev.off()

e='CYP71E1'
genes=expr[expr$qid==e,]$locus
main=paste0(e,"-like")
showExprVals(rpkm[genes,],NULL,main=main,plx='topleft',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
file=file.path(folder,paste0(e,'.tif'))
quartz(file=file,dpi=600,width=6,height=6,type="tiff")
showExprVals(rpkm[genes,],NULL,main=main,plx='topleft',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
dev.off()

e='UGT85B1'
genes=expr[expr$qid==e,]$locus
main=paste0(e,"-like")
showExprVals(rpkm[genes,],NULL,main=main,plx='topright',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
file=file.path(folder,paste0(e,'.tif'))
quartz(file=file,dpi=600,width=6,height=6,type="tiff")
showExprVals(rpkm[genes,],NULL,main=main,plx='topright',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
dev.off()

e='dhurrinase'
genes=expr[grep('dhurrinase',expr$qid),]$locus
main=paste0(e,"-like")
showExprVals(rpkm[genes,],NULL,main=main,plx='topleft',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
file=file.path(folder,paste0(e,'.tif'))
quartz(file=file,dpi=600,width=6,height=6,type="tiff")
showExprVals(rpkm[genes,],NULL,main=main,plx='topleft',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
dev.off()

e='hydroxynitrile lyase'
genes=expr[grep('hydroxynitrile',expr$qid),]$locus
main=paste0(e,"-like")
showExprVals(rpkm[genes,],NULL,main=main,plx='topright',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
file=file.path(folder,paste0(sub(' ','-',e),'.tif'))
quartz(file=file,dpi=600,width=6,height=6,type="tiff")
showExprVals(rpkm[genes,],NULL,main=main,plx='topright',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
dev.off()

e='AtNIT4'
genes=expr[expr$qid==e,]$locus
main=paste0(e,"-like")
showExprVals(rpkm[genes,],NULL,main=main,plx='topleft',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
file=file.path(folder,paste0(e,'.tif'))
quartz(file=file,dpi=600,width=6,height=6,type="tiff")
showExprVals(rpkm[genes,],NULL,main=main,plx='topleft',
             leg.cex=1,cols=brew,labels=labels,
             legend=T,ylab="Expression (RPKM)")
dev.off()
par(mfrow=c(1,1))
```


Limitations of this analysis
============================

Gene expression and enzyme levels may not correlate. 

Session Information
===================

```{r}
sessionInfo()
```
