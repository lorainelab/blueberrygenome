```{r echo=FALSE}
pwy.id='PWY-1422'
pwy.name='Vitamin E biosynthesis'
```


Vitamin E
==========


Analysis
--------

Load data for analysis:

```{r}
source('src/pathway_funcs.R')
genes=getPathwayGenes(pwy.id)
pwys=getPwyAnnotations()
rxn=getRxn(pwys)[genes]
annots=getAnnots()
rpkm=getRPKM()
expr=rpkm[genes,]
```

There were `r length(genes)` genes annotated to this pathway.

How many isozymes are there?

```{r}
rxns=table(rxn)
rxns
```

Plot expression:

```{r fig.width=5,fig.height=5}
library(RColorBrewer)
scheme="Dark2"
brew=brewer.pal(length(rxns),scheme)
```