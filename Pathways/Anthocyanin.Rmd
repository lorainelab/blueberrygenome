Introduction
------------

This Markdown explores genes involved in anthocyanin biosynthesis.

Questions we aim to answer include:

* Which genes annotated to anthocyanin biosynthesis are highest expressed in later stages of fruit ripening?

Analysis
--------

Getting the pathway ids and pathway names:

```{r}
# See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?object=ANTHOCYANIN-SYN
pwys=c('PWY-5125','PWY-5127','PWY-5139','PWY-6062','PWY-5313-ARA','PWY-5310','PWY-5312','PWY-5153','PWY-5286','PWY-5160','PWY-5268','PWY-5284','PWY-5295')
pwy.names=c('anthocyanin biosynthesis (pelargonidin 3-O-glucoside, cyanidin 3-O-glucoside)','gentiodelphin biosynthesis','pelargonidin conjugates biosynthesis','peonidin and derivatives biosynthesis','superpathway of anthocyanin biosynthesis (from cyanidin and cyanidin 3-O-glucoside)','superpathway of anthocyanin biosynthesis (from delphinidin 3-O-glucoside)','superpathway of anthocyanin biosynthesis (from pelargonidin 3-O-glucoside)','anthocyanin biosynthesis (delphinidin 3-O-glucoside)','anthocyanidin sophoroside metabolism','rose anthocyanin biosynthesis','salvianin biosynthesis','shisonin biosynthesis','ternatin C5 biosynthesis')
```

Load functions:

```{r}
source('src/pathway_funcs.R')
```

Get all the genes:

```{r}
genes=getPathwayGenes(pwys)
pwy_annots=getPwyAnnotations()
rxns=getRxn(pwy_annots)[genes]
ecs=getEC()[genes]
annots=getAnnots()
rpkm=getRPKM()
scaled.rpkm=getScaledRPKM()
expr=rpkm[genes,]
ec=getEC()[genes]
```

There were `r length(genes)` genes annotated to these pathways.

How many EC numbers were there?

```{r}
table(ecs)
```

Plotting expression of genes by EC number.

```{r}
# anthocyanidin glucoside glucosyltransfersae
gg='EC-2.4.1'
g='EC-2.4.1.115'
cd='EC-1.14.11.19'
```

Start with `r cd`:

```{r}
main='cyanidin or dihydroquercetin synthase'
EC=cd
gs=names(ecs[ecs==EC])
expr=rpkm[gs,]
showExprVals(expr,ecs[gs],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
# Make figure for publication:
folder='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Anthocyanin',files)],'/',EC,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=5,height=5,
       type="tiff")
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
dev.off()
annots[gs]
```

Next, `r g`:

```{r}
# http://www.enzyme-database.org/query.php?ec=2.4.1.115
main='anthocyanidin 3-O-glucosyltransferase'
EC=g
allgs=names(ecs[ecs==EC])
highgs=c('CUFF.20951','CUFF.35538')
othergs=allgs[allgs!=highgs]
gs=highgs
expr=rpkm[gs,]
showExprVals(expr,ecs[gs],main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
# Make figure for publication:
folder='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Anthocyanin',files)],'/',EC,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=5,height=5,
       type="tiff")
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
dev.off()
annots[gs]
```

```{r}
#http://enzyme.expasy.org/EC/2.4.1.-
```






Plot expression of enzymes by reaction
---------------------------------------

Note that some enzymes are annotated to multiple reactions, but only one is being shown below.


```{r fig.width=5,fig.height=5}
library(RColorBrewer) # http://bl.ocks.org/mbostock/5577023
scheme="Dark2"
brew=brewer.pal(length(table(ecs)),scheme)
labels=c("Pad","Cup","Green","Pink","Ripe")
```

### Leucocyanidin to dihydroquercitin catalyzed by leucoanthocyanidin dioxygenase EC 1.14.11.19 in pathway anthocyanin biosynthesis (pelargonin 3-O-glucoside, cyanidin 3-O-glucoside. 

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=1.14.11.19-RXN&redirect=T


```{r}
narxn='1.14.11.19-RXN'
pname='Dihydroquercetin synthesis'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')

# Make figure for publication:
folder='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Anthocyanin',files)],'/',rxn,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=5,height=5,
       type="tiff")
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
dev.off()
annots[gs]
```


### Glucosylation of pelargonindin in pathway anthocyanin biosynthesis (pelargonidin 3-O-glucoside, cyanidin 3-O-glucoside) EC 2.4.1.115

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=PELUDP-RXN&redirect=T

```{r}
rxn='PELUDP-RXN'
pname='pelargonidin 3-O-glucosylation'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
```

Note that expression levels are quite low, suggesting the endproduct pelargonidin may not be very abundant in ripe berries.

### Glucosylation of delphinidin by anthocyanidin 3-O-glucosyltransferase EC 2.4.1.115 in pathway  anthocyanin biosynthesis (delphinidin 3-O-glucoside) 

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=RXN-7815&redirect=T

```{r}
rxn='RXN-7815'
pname='delphinidin 3-O-glucosylation'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')

# Make figure for publication:
folder='../ManuscriptsSupplementalDataFiles/Figures'
files=dir(folder)
file=paste0(files[grep('Anthocyanin',files)],'/',rxn,'.tif')
file=file.path(folder,file)
quartz(file=file,dpi=600,width=5,height=5,
       type="tiff")
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
dev.off()
```



### Glucosylation of pelargonidin-3-O-beta-D-glucoside by anthocyanin 5-O-glucosyltransferase EC 2.4.1.- in pathway pelargonidin conjugates biosynthesis , salvianin biosynthesis  

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=RXN-7815&redirect=T

```{r}
rxn='RXN-7828'
pname='pelargonidin conjugates biosynthesis'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
```

### Glucosylation of cyanidin by anthocyanidin 5,3-O-glycosyltransferase EC 2.4.1.- in pathway rose anthocyanin biosynthesis 

The end product is cyanidin 5-O-β-D-glucoside.

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=RXN-7815&redirect=T

```{r}
rxn='RXN-8005'
pname=' rose anthocyanin biosynthesis'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
```

Expression of these enzymes is relatively low and also for two enzymes, it declines during ripening. So maybe this compound is abundant in immature fruits but less abundant in the later stages of ripening.

### Synthesis of cyanidin-3,5-diglucoside by glucosylation of cyanidin-3-O-β-D-glucoside by anthocyanin 5-O-glucosyltransferase EC 2.4.1.- in pathway shisonin biosynthesis , superpathway of anthocyanin biosynthesis (from cyanidin and cyanidin 3-O-glucoside) 

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=RXN-8169&redirect=T


```{r}
rxn='RXN-8169'
pname='cyanidin-3,5-diglucoside biosynthesis'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
```


### Synthesis of cyanidin 3-O-sophoroside from cyanidin-3-O-β-D-glucoside by anthocyanidin 3-O-glucoside 2"-O-glucosyltransferase Inferred from experiment  in pathways anthocyanidin sophoroside metabolism , superpathway of anthocyanin biosynthesis (from cyanidin and cyanidin 3-O-glucoside) , superpathway of anthocyanin biosynthesis (from cyanidin and cyanidin 3-O-glucoside) 

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=RXN-8176&redirect=T


```{r}
rxn='RXN-8176'
pname='cyanidin 3-O-sophoroside biosynthesis'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
```

Expression levels are not high, but it looks like expression increases in pink and ripe fruit, suggesting that the sophoroside conjugate is made.

### Synthesis of pelargonidin 3-O-sophoroside from pelargonidin-3-O-β-D-glucoside by anthocyanidin 3-O-glucoside 2"-O-glucosyltransferase Inferred from experiment  in pathway anthocyanidin sophoroside metabolism , superpathway of anthocyanin biosynthesis (from pelargonidin 3-O-glucoside) 

See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=RXN-8177&redirect=T


```{r}
rxn='RXN-8177'
pname='pelargonidin 3-O-sophoroside biosynthesis'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
```

Expression is low and declines during ripening. 

### Synthesis of cyanidin-3-O-β-D-glucoside from cyanidin by anthocyanidin 3-O-glucosyltransferase EC 2.4.1.115 in pathways  superpathway of anthocyanin biosynthesis (from cyanidin and cyanidin 3-O-glucoside) , anthocyanin biosynthesis (pelargonidin 3-O-glucoside, cyanidin 3-O-glucoside) , superpathway of anthocyanin biosynthesis (from cyanidin and cyanidin 3-O-glucoside) 


See: http://pmn.plantcyc.org/PLANT/NEW-IMAGE?type=NIL&object=RXN1F-775&redirect=T

```{r}
rxn='RXN1F-775'
pname='cyaniditabln-3-O-β-D-glucoside biosynthesis'
gs=names(rxns[rxns==rxn])
ec=ecs[gs]
main=paste0(rxn,': ',pname)
expr=rpkm[gs,]
showExprVals(expr,ec,main=main,legend=T,
             plx='topleft',labels=labels,
             leg.cex=1,ylab='Expression (RPKM)')
```

Expression is not high and it peaks at the mature green stage.






