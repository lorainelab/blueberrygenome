
big_blastx_plantcyc_results.tsv.gz:

Results from blastx of blueberry cDNAs against a database of plantcyc
enzymes downloaded in July 2013. The plantcyc enzymes fasta file was:

plantcyc.fasta.gz

And contained enzymes annotated to species. It is *not* the same thing
as the larger plantcyc enzymes file which contained enzyme seuqences for
some enzymes known to exist in plants but not yet annotated to a species
pathway (e.g. AraCyc). 

script used:

#!/bin/bash
#PBS -N big_blastx
#PBS -l nodes=1:ppn=6
#PBS -l vmem=16000mb
#PBS -l walltime=20:00:00
cd $PBS_O_WORKDIR

export BLASTDB=/lustre/groups/lorainelab/data/blueberry/90_plantcyc/fasta
blastx -query ${PBS_ARRAYID}.fa -out ${PBS_ARRAYID}.plantcyc.tsv -db plantcyc -evalue 
0.01 -max_target_seqs 1 -num_threads 6 -outfmt "7 qseqid qlen qframe qstart qend slen 
sstart send qcovs qcovhsp pident evalue bitscore ssid stitle"

job submitted with:

qsub -t 1-142 (there were 142 fasta files with 500 or fewer sequences)

cyc_pathways.tsv.gz:

Maps PlantCyc protein identifiers onto PlantCyc pathway, reaction,
pathway names, gene ids, etc. The final field reports the
species-level Cyc database they belong to. This is useful for linking
blastx results via query id to pathways.

plantcyc_enzymes.fasta.gz:

All PlantCyc enzymes, not just the ones in species-level Cyc databases.

plantcyc_pathways.20130723.gz

Same as cyc_pathways.tsv.gz, but doesn't contain the final field
reporting the species-level Cyc databases. This was downloaded
directly from PlantCyc.

PWY-861.fa:

Output of ../src/getPathwaySeqs.py, which retrieves fasta sequences
records for all the enzymes in a pathway. Depends on the fasta and
pathway files listed the the preceding sections. Probably we don't
need to version-control this because it's the output of a script that
processes other files, but I'm including it here to remind us that we
have something that can retrieve the sequences for a given pathway.