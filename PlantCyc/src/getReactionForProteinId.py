#!/usr/bin/env python

ex=\
"""
Kate Dreher explained that we can create a single .pf file for all the
blueberry proteins and provide this single .pf file as input to the
Pathway Tools Pathologic program, which will then be able to associate
our blueberry proteins with pathways and enzyme names.

The .pf file needs to contain the following fields:

ID - name of a gene (e.g., CUFF.123)
NAME - same as ID
PRODUCT-TYPE P
METACYC RXN-10430 (METACYC reaction id)
METACYC RXN-10431 
METACYC RXN-10432 (etc)

PathoLogic can also attempt to annotate using term matching with
GO terms and also free text descriptions from annotated sequences. However,
Kate does not recommend using this. Rather, it's better (she felt) to provide
the reaction ids OR the EC numbers, if no reaction id is available.

This script will attempt to create such a file, using results from parsing
the Markdown file 
"""

import sys,optparse,fileinput

def main(args):
    d = {}
    sep='\t'
    onfirst=True
    for line in fileinput.input(args):
        if onfirst:
            sys.stdout.write(line.rstrip()+'\tave.identity\n')
            onfirst=False
            continue
        toks=line.rstrip().split('\t')
        qid=toks[1]
        title=toks[14]
        if title=='NA':
            toks.append('NA')
            newline=sep.join(toks)+'\n'
            sys.stdout.write(line)
            continue
        if not d.has_key(qid):
            d[qid]=[toks]
        else:
            d[qid].append(toks)
    for qid in d.keys():
        hsps=d[qid]
        title=hsps[0][14]
        if title=='NA':
            hsp=hsp[0]
            line='\t'.join(hsp)+'\tNA\n'
            sys.stdout.write(line)
            break
        totalIdentity=0
        totalLength=0
        for hsp in hsps:
            start=int(hsp[7])
            end=int(hsp[8])
            length=end-start+1
            identity=float(hsp[11])
            totalLength=totalLength+length
            totalIdentity=totalIdentity+(identity*length)
        totalIdentity=totalIdentity/totalLength
        for hsp in hsps:
            line='\t'.join(hsp)+'\t'+str(totalIdentity)+'\n'
            sys.stdout.write(line)

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument('-f','--fasta_file',metavar='FASTA_FILE',
                        help='PlantCyc enzymes fasta file [required]')
    parser.add_argument('-p','--pathways_file',metavar='PATHWAYS_FILE',
                        help='PlantCyc pathways file [required]')
    parser.add_argument('files', metavar='FILE', nargs='*',
                        help='BLASTX output file, tab-delimited')
    args = parser.parse_args()
    main(fasta_file=args.files,
         pathways_file=args.pathways_file)
