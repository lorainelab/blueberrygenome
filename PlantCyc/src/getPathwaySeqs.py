#!/usr/bin/env python

ex=\
"""
Get fasta sequence for the given pathway.

"""

import sys,argparse,fileinput,gzip,Bio

def main(fasta_file=None,
         pathways_file=None,
         pwy=None):
    d = {}
    sep='\t'
    onfirst=True
    for line in fileinput.input(pathways_file):
        if line.startswith('Pathway-id'):
            continue
        toks=line.rstrip().split('\t')
        pwy_id=toks[0]
        if pwy_id==pwy:
            protein_id=toks[4]
            d[protein_id]=toks
    sys.stderr.write("Got %i proteins for pathway %s\n"%(len(d.keys()),pwy))
    from Bio import SeqIO
    if fasta_file.endswith('.gz'):
        handle=gzip.GzipFile(fasta_file)
    else:
        handle = open(fasta_file, "rU")
    for record in SeqIO.parse(handle, "fasta") :
        if d.has_key(record.id):
            SeqIO.write(record, sys.stdout, "fasta")
    handle.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=ex)
    parser.add_argument('-f','--fasta_file',nargs=1,metavar='FASTA_FILE',
                        help='PlantCyc enzymes fasta file [required]')
    parser.add_argument('-p','--pathways_file',nargs=1,metavar='PATHWAYS_FILE',
                        help='PlantCyc pathways file [required]')
    parser.add_argument('-P','--pwy',metavar='PWY',nargs=1,
                        help='Pathway id, e.g., PWY-861 [required]')
    args = parser.parse_args()
    fasta_file=args.fasta_file
    pathways_file=args.pathways_file
    pwy=args.pwy
    if fasta_file and pathways_file and pwy:
        main(fasta_file=fasta_file[0],
             pathways_file=pathways_file[0],
             pwy=pwy[0])
    else:
        parser.print_help()
