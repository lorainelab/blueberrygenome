#!/usr/bin/env python

ex=\
"""
To assess whether a hit to a database entry is real, we need a summarized
value for the overall percent identity of the entire hit. However, percent
identity is only reported for individual HSPs.

This script calculates a weighted average percent identity for the entire
hit, using the length of the region covered in the subject (the matched
protein sequence) to weight percent identity.

One limitation: sometimes HSPs overlap and some amino acids will contribute
more than once to the total average.

Assumptions:

Output contains one and only one subject per query.
Subject start is field 7.
Subject end is field 8.
Percent identity for an HSP is field 11.
Subject title is field 15.
"""

import sys,optparse,fileinput

def main(args):
    d = {}
    sep='\t'
    onfirst=True
    for line in fileinput.input(args):
        if onfirst:
            sys.stdout.write(line.rstrip()+'\tave.identity\n')
            onfirst=False
            continue
        toks=line.rstrip().split('\t')
        qid=toks[1]
        title=toks[14]
        if title=='NA':
            toks.append('NA')
            newline=sep.join(toks)+'\n'
            sys.stdout.write(line)
            continue
        if not d.has_key(qid):
            d[qid]=[toks]
        else:
            d[qid].append(toks)
    for qid in d.keys():
        hsps=d[qid]
        title=hsps[0][14]
        if title=='NA':
            hsp=hsp[0]
            line='\t'.join(hsp)+'\tNA\n'
            sys.stdout.write(line)
            break
        totalIdentity=0
        totalLength=0
        for hsp in hsps:
            start=int(hsp[7])
            end=int(hsp[8])
            length=end-start+1
            identity=float(hsp[11])
            totalLength=totalLength+length
            totalIdentity=totalIdentity+(identity*length)
        totalIdentity=totalIdentity/totalLength
        for hsp in hsps:
            line='\t'.join(hsp)+'\t'+str(totalIdentity)+'\n'
            sys.stdout.write(line)

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)

