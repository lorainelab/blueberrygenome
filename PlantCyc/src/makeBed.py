#!/usr/bin/env python

ex=\
"""
Annotates BED14 format file with pathway information. Saves annotation text
in field 14 (description) of a BED14 file.

Reads:
  Filtered PlantCyc results (ex. filter_plantcyc_results_60_45.tsv.gz)
  BED14 file to annotate. (ex. V_corymbosum_scaffold_May_2013_withDescr.bed.gz)

Emits:
  New BED14 file with pathway information added.

This is run by MakingAnnotationFiles.Rmd, which makes filtered PlantCyc results.
"""

import sys,optparse,fileinput,gzip

# writes new bed lines for gene models with
# pathway annotations, writes to stdout
# note: only one pathway annotation will be written as some
# models may be associated with multiple pathways
def main(pwyfile,bedfile):
    bed=readBedFile(bedfile)
    pwys=readPwyFile(pwyfile)
    for gene_id in bed.keys():
        if pwys.has_key(gene_id):
            updateDescr(gene_id,bed,pwys) # updates detail field in bed
        lines=map(lambda x:'\t'.join(x),bed[gene_id]) # a gene can have multiple gene models
        txt='\n'.join(lines)+'\n'
        sys.stdout.write(txt)

# read output from MakeingAnnotationFiles.Rmd
def readPwyFile(pwyfile):
    fh = openFile(pwyfile)
    annots=fh.readlines()
    fh.close()
    pwys={}
    for annot in annots:
        toks=annot.rstrip().split('\t')
        gene_id=toks[3]
        if pwys.has_key(gene_id):
            pwys[gene_id].append(toks)
        else:
            pwys[gene_id]=[toks]
    return pwys

# read bed detail file, return dictionary
# keys are gene ids, values are lists
# lists contain lists of fields from single bed
# line
def readBedFile(bedfile):
    fh = openFile(bedfile)
    models=fh.readlines()
    fh.close()
    bed={}
    for model in models:
        toks=model.rstrip().split('\t')
        gene_id=toks[12]
        if bed.has_key(gene_id):
            bed[gene_id].append(toks)
        else:
            bed[gene_id]=[toks]
    return bed

# open fname, can be .gz 
def openFile(fname):
    if fname.endswith('.gz'):
        fh=gzip.GzipFile(fname)
    else:
        fh=open(fname)
    return fh

"""
> names(filtered.hits.low)
 [1] "s.id"         "q.id"         "q.length"     "s.length"    
 [5] "ave.identity" "s.cov"        "protein"      "species"     
 [9] "Pathway.id"   "Pathway.name" "Reaction.id"  "EC"          
[13] "Protein.name" "Gene.id"      "Gene.name"    "cyc"
"""
def updateDescr(gene_id,bed,pwys):
    annots=pwys[gene_id]
    models=bed[gene_id]
    txs = {}
    # retrieve pathway annotations for each transcript
    # there could be more than one pathway annotaton per transcript
    for annot in annots:
        txid=annot[2] 
        if not txs.has_key(txid):
            txs[txid]=[annot]
        else:
            txs[txid].append(annot)
    for txid in txs.keys():
        subannots=txs[txid]
        sid=subannots[0][0]
        phid=subannots[0][1]
        ident=subannots[0][6]
        scov=subannots[0][7]
        protein=subannots[0][8]
        species=subannots[0][9]
        ec=subannots[0][13]
        new_descr='SIMILAR TO %s'%sid
        if not phid=='NA':
            new_descr='%s (%s)'%(new_descr,phid)
        if not ec=='NA':
            new_descr='%s %s'%(new_descr,ec)
        new_descr='%s %s (scov=%s,ident=%s)'%(new_descr,protein,scov,ident)
        pwy_ids=[]
        pwy_names=[]
        for annot in subannots:
            pwy_id = annot[10]
            if pwy_id == 'NA':
                break
            pwy_name=annot[11]
            if pwy_id not in pwy_ids:
                pwy_ids.append(pwy_id)
                pwy_names.append(pwy_name)
        # this ensures we report every pathway id - recall that an enzyme
        # can be shared across many pathway accessions, either because of
        # uncertainty about the enzyme's true function *or* because the enzyme
        # is multi-functional *or* because of uncertainty about the pathway
        # and its steps 
        if len(pwy_ids)>0:
            pwys=';'.join(map(lambda x,y:x+'|'+y,pwy_ids,pwy_names))
            new_descr='%s PlantCyc %s'%(new_descr,pwys)
        for model in models:
            if model[3]==txid:
                model[13]=new_descr

if __name__ == '__main__':
    usage='%prog [options]\n'+ex
    parser=optparse.OptionParser(usage)
    parser.add_option('-p','--pathway_info',dest='pwys',
                      help='Pathway and enzyme annotations file [required]',
                      metavar='PWY')
    parser.add_option('-b','--bedfile',dest='bedfile',
                      help='BED14 file with gene name in field 13 [required]',
                      metavar='BEDFILE')
    (options,args)=parser.parse_args()
    if not options.pwys or not options.bedfile:
        parser.print_help()
    else:
        main(options.pwys,options.bedfile)

