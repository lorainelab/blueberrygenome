#!/usr/bin/env python

ex=\
"""
To assess whether a hit to a database entry is real, we need to know how much
of the matched (subject) protein was covered by the HSPs comprising the hit.
However, often times the HSPs overlap along the subject sequence, making it
difficult to compute percent coverage for a given subject. This script reads
blastx output from the big_blastx.py pipeline and calculates percent subject
coverage for each query and subject pair, adding the percent coverage as a final
field to every HSP.

Assumptions:

Output contains one and only one subject per query.
Subject start is field 7.
Subject end is field 8.
Subject title is field 15.
"""

import sys,optparse,fileinput

def main(args):
    d = {}
    sep='\t'
    onfirst=True
    for line in fileinput.input(args):
        if onfirst:
            sys.stdout.write(line.rstrip()+'\ts.cov\n')
            onfirst=False
            continue
        toks=line.rstrip().split('\t')
        qid=toks[1]
        title=toks[14]
        if title=='NA':
            toks.append('NA')
            newline=sep.join(toks)+'\n'
            sys.stdout.write(line)
            continue
        if not d.has_key(qid):
            d[qid]=[toks]
        else:
            d[qid].append(toks)
    for qid in d.keys():
        hsps=d[qid]
        title=hsps[0][14]
        slen=int(hsps[0][6])
        s=[0]*slen # make empty array of size slen
        for hsp in hsps:
            stitle=hsp[14]
            if not title==stitle:
                raise ValueErorr("Two different subjects per query %s"%qid)
            start=int(hsp[7])-1
            end=int(hsp[8])
            for i in range(start,end):
                s[i]=1
        scov=sum(s)/float(slen)*100
        for hsp in hsps:
            line='\t'.join(hsp)+'\t'+str(scov)+'\n'
            sys.stdout.write(line)

if __name__ == '__main__':
    usage='%prog [ file ... ]\n'+ex
    parser=optparse.OptionParser(usage)
    (options,args)=parser.parse_args()
    main(args=args)

